#!/usr/bin/env python

from distutils.core import setup

setup(name='pkg',
      version='1.0',
      description='Mon Package',
      author='Francois Belisle',
      author_email='f@mathmobile.io',
      url='https://bitbucket.org/fbelisle/remote_test/src/master/pkg/',
      packages=['pkg'],
     )
